package model;

public class Visual_parameters {

  private String color_of_stem;
  private String color_of_leaf;
  private double average_size;

  public Visual_parameters(){
  }

  public Visual_parameters(String color_of_stem, String color_of_leaf, double average_size) {
    this.color_of_stem = color_of_stem;
    this.color_of_leaf = color_of_leaf;
    this.average_size = average_size;
  }

  public String getColor_of_stem() {
    return color_of_stem;
  }

  public void setColor_of_stem(String color_of_stem) {
    this.color_of_stem = color_of_stem;
  }

  public String getColor_of_leaf() {
    return color_of_leaf;
  }

  public void setColor_of_leaf(String color_of_leaf) {
    this.color_of_leaf = color_of_leaf;
  }

  public double getAverage_size() {
    return average_size;
  }

  public void setAverage_size(double average_size) {
    this.average_size = average_size;
  }

  @Override
  public String toString() {
    return "{"+
        "color_of_stem='" + color_of_stem + '\'' +
        ", color_of_leaf='" + color_of_leaf + '\'' +
        ", average_size=" + average_size +
        '}';
  }
}
