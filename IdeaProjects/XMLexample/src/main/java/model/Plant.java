package model;

import java.util.List;
import org.w3c.dom.NodeList;

public class Plant {

  private int plantNo;
  private String name;
  private String soil;
  private String origin;
  private String multiplying;
  private Visual_parameters visual_parameters;
  private List<Growing_tips> growing_tips;

  public Plant() {
  }

  public Plant(int plantNo, String name, String  soil, String origin,
      String multiplying, Visual_parameters visual_parameters, List<Growing_tips> growing_tips) {
    this.plantNo = plantNo;
    this.name = name;
    this.soil = soil;
    this.origin = origin;
    this.multiplying = multiplying;
    this.visual_parameters = visual_parameters;
    this.growing_tips = growing_tips;
  }

  public int getPlantNo() {
    return plantNo;
  }

  public void setPlantNo(int plantNo) {
    this.plantNo = plantNo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSoil() {
    return soil;
  }

  public void setSoil(String soil) {
    this.soil = soil;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String  getMultiplying() {
    return multiplying;
  }

  public void setMultiplying(String multiplying) {
    this.multiplying = multiplying;
  }

  public Visual_parameters getVisual_parameters() {
    return visual_parameters;
  }

  public void setVisual_parameters(Visual_parameters visual_parameters) {
    this.visual_parameters = visual_parameters;
  }

  public List<Growing_tips> getGrowing_tips() {
    return growing_tips;
  }

  public void setGrowing_tips(List<Growing_tips> growing_tips) {
    this.growing_tips = growing_tips;
  }

  @Override
  public String toString() {
    return "Plant{" +
        "plantNo=" + plantNo +
        ", name='" + name + '\'' +
        ", soil=" + soil +
        ", origin='" + origin + '\'' +
        ", multiplying=" + multiplying +
        ", visual_parameters=" + visual_parameters +
        ", growing_tips=" + growing_tips +
        '}';
  }
}
