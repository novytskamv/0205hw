package model;

public class Growing_tips {

  private double temperature;
  private boolean isLikeLight;
  private int watering;

  public Growing_tips() {
  }

  public Growing_tips(double temperature, boolean isLikeLight, int watering) {
    this.temperature = temperature;
    this.isLikeLight = isLikeLight;
    this.watering = watering;
  }

  public double getTemperature() {
    return temperature;
  }

  public void setTemperature(double temperature) {
    this.temperature = temperature;
  }

  public boolean isLikeLight() {
    return isLikeLight;
  }

  public void setLikeLight(boolean likeLight) {
    isLikeLight = likeLight;
  }

  public int getWatering() {
    return watering;
  }

  public void setWatering(int watering) {
    this.watering = watering;
  }

  @Override
  public String toString() {
    return "{" +
        "temperature=" + temperature +
        ", isLikeLight=" + isLikeLight +
        ", watering=" + watering +
        '}';
  }
}
