package parcer.stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import model.Growing_tips;
import model.Plant;
import model.Visual_parameters;

public class STAXReader {

  public static List<Plant> parsePlant(File xml, File xsd){
    List<Plant> plantList = new ArrayList<>();
    Plant plant = null;
    Visual_parameters visual_parameters = null;
    Growing_tips gt = null;
    List<Growing_tips> growing_tips = null;

    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = inputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()){
        XMLEvent event = xmlEventReader.nextEvent();
        if(event.isStartElement()){
          StartElement startElement = event.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name){
            case "plant": plant = new Plant();
              Attribute attribute = startElement.getAttributeByName(new QName("plantNo"));
              if(attribute !=null){
                plant.setPlantNo(Integer.parseInt(attribute.getValue()));
              }
              break;
            case "name":
              event = xmlEventReader.nextEvent();
              assert plant !=null;
              plant.setName(event.asCharacters().getData());
              break;
            case "soil":
              event = xmlEventReader.nextEvent();
              assert plant != null;
              plant.setSoil(event.asCharacters().getData());
              break;
            case "origin":
              event = xmlEventReader.nextEvent();
              assert plant != null;
              plant.setOrigin(event.asCharacters().getData());
              break;
            case "multiplying":
              event = xmlEventReader.nextEvent();
              assert plant !=null;
              plant.setMultiplying(event.asCharacters().getData());
              break;
            case "visual_parameters":
              event = xmlEventReader.nextEvent();
              visual_parameters = new Visual_parameters();
              plant.setVisual_parameters(visual_parameters);
              break;
            case "color_of_stem":
              event = xmlEventReader.nextEvent();
              assert visual_parameters != null;
              visual_parameters.setColor_of_stem(event.asCharacters().getData());
              break;
            case "average_size":
              event = xmlEventReader.nextEvent();
              assert visual_parameters != null;
              visual_parameters.setAverage_size(Double.parseDouble(event.asCharacters().getData()));
              break;
            case "color_of_leaf":
              event = xmlEventReader.nextEvent();
              assert visual_parameters != null;
              visual_parameters.setColor_of_leaf(event.asCharacters().getData());
              break;
            case "growing_tips":
              event = xmlEventReader.nextEvent();
              growing_tips = new ArrayList<>();
              gt = new Growing_tips();
              growing_tips.add(gt);
              plant.setGrowing_tips(growing_tips);
              break;
            case "temperature":
              event = xmlEventReader.nextEvent();
              assert growing_tips != null;
              gt.setTemperature(Double.parseDouble(event.asCharacters().getData()));
              break;
            case "isLikeLight":
              event = xmlEventReader.nextEvent();
              assert growing_tips != null;
              gt.setLikeLight(Boolean.parseBoolean(event.asCharacters().getData()));
              break;
            case "watering":
              event = xmlEventReader.nextEvent();
              assert growing_tips != null;
              gt.setWatering(Integer.parseInt(event.asCharacters().getData()));
              break;
          }
        }
        if(event.isEndElement()){
          EndElement endElement = event.asEndElement();
          if(endElement.getName().getLocalPart().equals("plant")){
            plantList.add(plant);
          }
        }
      }
    } catch (XMLStreamException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return plantList;
  }

}
