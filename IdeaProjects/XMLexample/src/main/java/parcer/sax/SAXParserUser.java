package parcer.sax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import model.Plant;

public class SAXParserUser {
  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Plant> parsePlant(File xml, File xsd){
    List<Plant> plantList = new ArrayList<>();
    try {
      saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

      SAXParser saxParser = saxParserFactory.newSAXParser();
      SAXHandler saxHandler = new SAXHandler();
      saxParser.parse(xml, saxHandler);

      plantList = saxHandler.getPlantList();
    }catch (SAXException | ParserConfigurationException | IOException ex){
      ex.printStackTrace();
    }

    return plantList;
  }
}

