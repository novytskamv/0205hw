package parcer.sax;

import java.util.ArrayList;
import java.util.List;
import model.Growing_tips;
import model.Plant;
import model.Visual_parameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {

  private List<Plant> plantList = new ArrayList<>();
  private Plant plant = null;
  private Visual_parameters visual_parameters = null;
  private List<Growing_tips> growing_tips;
  private Growing_tips gt;

  private boolean bName = false;
  private boolean bSoil = false;
  private boolean bOrigin = false;
  private boolean bMultiplying = false;
  private boolean bVisual_parameters = false;
  private boolean bColor_of_stem = false;
  private boolean bColor_of_leaf = false;
  private boolean bAverage_size = false;
  private boolean bGrowing_tips = false;
  private boolean bTemperature = false;
  private boolean bIsLikeLight = false;
  private boolean bWatering = false;

  public List<Plant> getPlantList(){
    return this.plantList;
  }

  public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
    if(qName.equalsIgnoreCase("plant")){
      String plantN = attributes.getValue("plantNo");
      plant = new Plant();
      plant.setPlantNo(Integer.parseInt(plantN));
    }
    else if (qName.equalsIgnoreCase("name")){bName = true;}
    else if (qName.equalsIgnoreCase("soil")){bSoil = true;}
    else if (qName.equalsIgnoreCase("origin")){bOrigin = true;}
    else if (qName.equalsIgnoreCase("multiplying")){bMultiplying = true;}
    else if (qName.equalsIgnoreCase("visual_parameters")){bVisual_parameters = true;}
    else if (qName.equalsIgnoreCase("color_of_stem")){bColor_of_stem = true;}
    else if (qName.equalsIgnoreCase("color_of_leaf")){bColor_of_leaf = true;}
    else if (qName.equalsIgnoreCase("average_size")){bAverage_size = true;}
    else if (qName.equalsIgnoreCase("growing_tips")){bGrowing_tips = true;}
    else if (qName.equalsIgnoreCase("temperature")){bTemperature = true;}
    else if (qName.equalsIgnoreCase("isLikeLight")){bIsLikeLight = true;}
    else if (qName.equalsIgnoreCase("watering")){bWatering = true;}
  }

  public void endElement(String uri, String localName, String qName) throws SAXException{
    if(qName.equalsIgnoreCase("plant")){
      plantList.add(plant);
    }
  }

  public void characters(char ch[], int start, int length) throws SAXException{
    if(bName){
      plant.setName(new String(ch, start, length));
      bName = false;
    }
    else if(bSoil){
      plant.setSoil(new String(ch, start, length));
      bSoil = false;
    }
    else if(bOrigin){
      plant.setOrigin(new String(ch, start, length));
      bOrigin = false;
    }
    else if(bMultiplying){
      plant.setMultiplying(new String(ch, start, length));
      bMultiplying = false;
    }
    else if(bVisual_parameters){
      visual_parameters = new Visual_parameters();
      plant.setVisual_parameters(visual_parameters);
      bVisual_parameters = false;
    }
    else if(bColor_of_stem){
      String str = new String(ch, start, length);
      visual_parameters.setColor_of_stem(str);
      bColor_of_stem = false;
    }
    else if(bColor_of_leaf){
      visual_parameters.setColor_of_leaf(new String(ch, start, length));
      bColor_of_leaf = false;
    }
    else if(bAverage_size){
      visual_parameters.setAverage_size(Double.parseDouble(new String(ch, start, length)));
      bAverage_size = false;
    }
    else if(bGrowing_tips){
      growing_tips = new ArrayList<>();
      gt = new Growing_tips();
      growing_tips.add(gt);
      plant.setGrowing_tips(growing_tips);
      bGrowing_tips = false;
    }
    else if(bTemperature){
      gt.setTemperature(Double.parseDouble(new String(ch, start, length)));
      bTemperature = false;
    }
    else if(bIsLikeLight){
      gt.setLikeLight(Boolean.parseBoolean(new String(ch, start, length)));
      bIsLikeLight = false;
    }
    else if(bWatering){
      gt.setWatering(Integer.parseInt(new String(ch, start, length)));
      bWatering = false;
    }
  }
}
