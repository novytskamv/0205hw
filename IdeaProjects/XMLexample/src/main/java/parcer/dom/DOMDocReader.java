package parcer.dom;

import java.util.ArrayList;
import java.util.List;
import model.Growing_tips;
import model.Plant;
import model.Visual_parameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMDocReader {

  public List<Plant> readDoc(Document doc){
    doc.getDocumentElement().normalize();
    List<Plant> plants = new ArrayList<>();

    NodeList nodeList = doc.getElementsByTagName("plant");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Plant plant = new Plant();
      Visual_parameters visual_parameters;
      List<Growing_tips> growing_tips;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE){
        Element element = (Element) node;

        plant.setPlantNo(Integer.parseInt(element.getAttribute("plantNo")));
        plant.setName(element.getElementsByTagName("name").item(0).getTextContent());
        plant.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
        plant.setSoil(element.getElementsByTagName("soil").item(0).getTextContent());
        plant.setMultiplying(element.getElementsByTagName("multiplying").item(0).getTextContent());

        visual_parameters = getVisual_parametrs(element.getElementsByTagName("visual_parameters"));
        growing_tips = getGrowing_tips(element.getElementsByTagName("growing_tips"));

        plant.setVisual_parameters(visual_parameters);
        plant.setGrowing_tips(growing_tips);

        plants.add(plant);
      }
    }
    return plants;
  }

  private Visual_parameters getVisual_parametrs(NodeList nodes){
    Visual_parameters visual_parameters= new Visual_parameters();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
      Element element = (Element) nodes.item(0);
      visual_parameters.setAverage_size(Double.parseDouble(element.getElementsByTagName("average_size").item(0).getTextContent()));
      visual_parameters.setColor_of_leaf(element.getElementsByTagName("color_of_leaf").item(0).getTextContent());
      visual_parameters.setColor_of_stem(element.getElementsByTagName("color_of_stem").item(0).getTextContent());
    }

    return visual_parameters;
  }

  private List<Growing_tips> getGrowing_tips(NodeList nodes){
    List<Growing_tips> growing_tips = new ArrayList<>();
    Growing_tips growing_tips1 = new Growing_tips();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("temperature")) {
          Element el = (Element) node;
          growing_tips1.setTemperature(Double.parseDouble(el.getTextContent()));
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("isLikeLight")) {
          Element el = (Element) node;
          growing_tips1.setLikeLight(Boolean.parseBoolean(el.getTextContent()));
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("watering")) {
          Element el = (Element) node;
          growing_tips1.setWatering(Integer.parseInt(el.getTextContent()));
        }
      }
      growing_tips.add(growing_tips1);
    }
    return growing_tips;
  }

}
