package parcer.dom;

import java.io.File;
import java.util.List;
import model.Plant;
import org.w3c.dom.Document;

public class DOMParcerUser {

  public static List<Plant> getPlantList(File xml, File xsd){
    DOMDocCreator creator = new DOMDocCreator(xml);
    Document doc = creator.getDocument();


    DOMDocReader reader = new DOMDocReader();

    return reader.readDoc(doc);
  }

}
