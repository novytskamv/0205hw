package comparator;

import java.util.Comparator;
import model.Plant;

public class PlantComparator implements Comparator<Plant> {

  @Override
  public int compare(Plant t1, Plant t2) {
    return Double.compare(t1.getVisual_parameters().getAverage_size(), t2.getVisual_parameters().getAverage_size());
  }
}
