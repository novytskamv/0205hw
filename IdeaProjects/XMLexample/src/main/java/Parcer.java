import filechecker.ExtensionChecker;
import java.io.File;
import java.util.List;
import model.Plant;
import parcer.dom.DOMParcerUser;
import parcer.sax.SAXParserUser;
import parcer.stax.STAXReader;

public class Parcer {

  public static void main(String[] args) {

    File xml = new File("/home/kitty/plantXML.xml");
    File xsd = new File("/home/kitty/plantXML.xsd");

    if(checkIfXML(xml) && checkIfXSD(xsd)){
      System.out.println("\nDOM Parser: ");
      printList(DOMParcerUser.getPlantList(xml, xsd));
      System.out.println("\nSAX Parser: ");
      printList(SAXParserUser.parsePlant(xml, xsd));
      System.out.println("\nSTAX Parser: ");
      printList(STAXReader.parsePlant(xml, xsd));

    }
  }

  private static boolean checkIfXSD(File xsd) {
    return ExtensionChecker.isXSD(xsd);
  }

  private static boolean checkIfXML(File xml) {
    return ExtensionChecker.isXML(xml);
  }

  private static void printList(List<Plant> plants) {
    for (Plant plant: plants) {
      System.out.println(plant);
    }
  }

}
