<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          table.tfmt {
          border: 1px ;
          }

          td.colfmt {
          border: 1px ;
          background-color: yellow;
          color: red;
          text-align:right;
          }

          th {
          background-color: #2E9AFE;
          color: white;
          }

        </style>
      </head>

      <body>
        <table class="tfmt">
          <tr>
            <th style="width:250px">name</th>
            <th style="width:250px">soil</th>
            <th style="width:250px">origin</th>
            <th style="width:250px">multiplying</th>
            <th style="width:250px">visual_parameters</th>
            <th style="width:250px">color_of_stem</th>
            <th style="width:250px">color_of_leaf</th>
            <th style="width:250px">average_size</th>
            <th style="width:250px">growing_tips</th>
            <th style="width:250px">temperature</th>
            <th style="width:250px">isLikeLight</th>
            <th style="width:250px">watering</th>

          </tr>

          <xsl:for-each select="plants/plant">

            <tr>
              <td class="colfmt">
                <xsl:value-of select="@plantNo"/>
              </td>

              <td class="colfmt">
                <xsl:value-of select="name" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="soil" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="origin" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="multiplying" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="visual_parameters" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="average_size" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="color_of_leaf" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="average_size" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="growing_tips" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="temperature" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="isLikeLight" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="watering" />
              </td>
            </tr>

          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>