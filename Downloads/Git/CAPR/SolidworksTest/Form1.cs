﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swcommands;
using SolidWorks.Interop.swconst;
using System.Diagnostics;

namespace SolidworksTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SldWorks SwApp;
        IModelDoc2 swModel;

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process[] processes = Process.GetProcessesByName("SLDWORKS");
            foreach (Process process in processes)
            {
                process.CloseMainWindow();
                process.Kill();
            }

            SwApp = new SldWorks();
            SwApp.Visible = true;


            SwApp.NewPart();

            swModel = SwApp.IActiveDoc2;

            //=====================================================================================
            //Rect parallelepiped
            //outer rect parallelepiped
            swModel.Extension.SelectByID2("Front Plane", "PLANE", 0, 0, 0, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCornerRectangle(-0.14, -0.02, 0, 0, 0.04, 0);
            swModel.FeatureManager.FeatureExtrusion2(true, false, false, 0, 0, 0.03, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, 0, 0, false);

            //inner rect parallelepiped
            swModel.Extension.SelectByID2("", "FACE", 0, 0, 0, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCornerRectangle(0.14, -0.02, 0, 0.14 - 0.07, -0.02 + 0.05, 0);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.02, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small hole 1 in rect parallelepiped
            swModel.Extension.SelectByID2("", "FACE", 0, 0, 0.03, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(-0.14 + 0.02, -0.02 + 0.06 - 0.025, 0, 0.015 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.01, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small hole 2 in rect parallelepiped
            swModel.Extension.SelectByID2("", "FACE", 0, 0, 0.03, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(-0.14 + 0.05, -0.02 + 0.06 - 0.025, 0, 0.015 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.01, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //=====================================================================================
            //Cylinder
            //outer shell cylinder
            swModel.Extension.SelectByID2("Front Plane", "PLANE", 0, 0, 0, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, 0, 0, 0.08 / 2);
            swModel.FeatureManager.FeatureExtrusion2(true, false, false, 0, 0, 0.06, 0, false, false, false, false, 0, 0, false, false, false, false, true, true, true, 0, 0, false);

            //big inner cylinder
            swModel.Extension.SelectByID2("", "FACE", 0, 0, 0.06, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, 0, 0, 0.04 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.04, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small inner cylinder
            swModel.Extension.SelectByID2("", "FACE", 0, 0, 0.02, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, 0, 0, 0.03 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.02, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small hole 1 in cylinder
            swModel.Extension.SelectByID2("", "FACE", 0.055 / 2, 0, 0.06, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0.055 / 2, 0, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.02, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            swModel.Extension.SelectByID2("", "FACE", 0.055 / 2, 0, 0.04, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0.055 / 2, 0, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.01, 0, true, false, false, false, 0.78539816339745, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small hole 2 in cylinder
            swModel.Extension.SelectByID2("", "FACE", -0.055 / 2, 0, 0.06, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(-0.055 / 2, 0, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.02, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            swModel.Extension.SelectByID2("", "FACE", -0.055 / 2, 0, 0.04, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(-0.055 / 2, 0, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.01, 0, true, false, false, false, 0.78539816339745, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small hole 3 in cylinder
            swModel.Extension.SelectByID2("", "FACE", 0, 0.055 / 2, 0.06, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, 0.055 / 2, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.02, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            swModel.Extension.SelectByID2("", "FACE", 0, 0.055 / 2, 0.04, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, 0.055 / 2, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.01, 0, true, false, false, false, 0.78539816339745, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //small hole 4 in cylinder
            swModel.Extension.SelectByID2("", "FACE", 0, -0.055 / 2, 0.06, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, -0.055 / 2, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.02, 0, false, false, false, false, 0, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            swModel.Extension.SelectByID2("", "FACE", 0, -0.055 / 2, 0.04, false, 0, null, 0);
            swModel.SketchManager.InsertSketch(true);
            swModel.SketchManager.CreateCircleByRadius(0, -0.055 / 2, 0, 0.01 / 2);
            swModel.FeatureManager.FeatureCut3(true, false, false, 0, 0, 0.01, 0, true, false, false, false, 0.78539816339745, 0, false, false, false, false, false, true, true, true, true, false, 0, 0, false);

            //save
            //swModel.SaveAs3("C:\\1.SLDPRT",0 ,2);


        }
    }
}
